= JavaFX AOT GraalVM Hints

== Setup

[source,groovy]
----
repositories {
	maven {
		url 'https://gitlab.com/api/v4/projects/43419356/packages/maven'
	}
}

dependencies {
	implementation 'net.poundex.javafx:javafx-graalvm-hints:0.1.0'
}
----